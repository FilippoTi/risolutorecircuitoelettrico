package GUICOMP;

import javax.swing.JTextField;

public class Caselle extends JTextField
{
    JTextField caselle[][];
    int nR, nC;
    
    public Caselle(int nR, int nC)
    {
        this.caselle = new JTextField[nR][nC];
        this.nR = nR;
        this.nC = nC;        
    }
    
}
