package risolutorecircuitoelettrico.Classi;

public interface SistemaLineare
{

    /**
     * Risolve un sistema lineare
     *
     * @param TerminiNoti
     * @return la matrice X
     */
    public Matrice Risolvi(Matrice TerminiNoti);
}
