package risolutorecircuitoelettrico.Classi;

import java.io.*;
import java.util.*;

public class Matrice
{

    /**
     * Numero di righe che compongono la matrice
     */
    protected int NumeroDiRighe;

    /**
     * Numero di colonne che compongono la matrice
     */
    protected int NumeroDiColonne;

    /**
     * La matrice effettiva
     */
    protected double matrice[][];

    /**
     * Costruttore di default: Imposto le dimensioni a 1 1 e imposto l'elemento
     * a 0.0
     *
     * @since 11/10/2019
     */
    public Matrice()
    {
        this.matrice = new double[1][1];
        matrice[0][0] = 0.0;
        this.NumeroDiRighe = 1;
        this.NumeroDiColonne = 1;
    }

    /**
     * Costruttore: Imposto le dimensioni della matrice passata come paramentro
     * e copio i valori
     *
     * @param matrice la matrice dalla quale prelevo i valori e le dimensioni
     * @since 10/10/2019
     */
    public Matrice(double[][] matrice)
    {
        if (matrice.length > 0 && matrice[0].length > 0)
        {
            this.matrice = matrice;
            this.NumeroDiRighe = matrice.length;
            this.NumeroDiColonne = matrice[0].length;
        }
    }

    /**
     * Costruttore: Imposto le dimensioni della matrice e imposto i valori a
     * zero
     *
     * @param NumeroRighe numero di righe
     * @param NumeroColonne numero di colonne
     * @since 25/10/2019
     */
    public Matrice(int NumeroRighe, int NumeroColonne)
    {
        this.matrice = new double[NumeroRighe][NumeroColonne];
        this.NumeroDiRighe = NumeroRighe;
        this.NumeroDiColonne = NumeroColonne;

        int i, j;
        for (i = 0; i < this.NumeroDiRighe; i++)
        {
            for (j = 0; j < this.NumeroDiColonne; j++)
            {
                this.matrice[i][j] = 0.0;
            }
        }
    }

    //////////////////////////////////////////////////////////////////// SETGET
    /**
     * Ritorna il numero di righe che compongono la matrice
     *
     * @since 12/10/2019
     * @return il numero di righe della matrice
     */
    public int getNumeroDiRighe()
    {
        return this.NumeroDiRighe;
    }

    /**
     * Ritorna il numero di colonne che compongono la matrice
     *
     * @since 11/10/2019
     * @return il numero di colonne della matrice
     */
    public int getNumeroDiColonne()
    {
        return this.NumeroDiColonne;
    }

    /**
     * Ritorna l'elemento corrispondente alla cella Riga x Colonna
     *
     * @param Riga La riga da dove prelevare il valore
     * @param Colonna La colonna da dove prelevare il valore
     * @return L'elemento corrispondente alla cella Riga x Colonna, 0.0 se la
     * cella non esiste
     * @since 11/10/2019
     */
    public double getElemento(int Riga, int Colonna)
    {
        if (Riga < this.getNumeroDiRighe() && Colonna < this.getNumeroDiColonne())
        {
            return this.matrice[Riga][Colonna];
        }

        return 0.0;
    }

    /**
     * Imposta il numero di righe e di colonne che compongono la matrice, se il
     * numero di righe o di colonne è minore di 1 ritorna un messaggio di errore
     * e non imposta il numero di righe e di colonne
     *
     * @param Righe il numero di righe che compongono la matrice
     * @param Colonne il numero di colonne che compongono la matrice
     * @return 0 se le dimensioni sono accettabili, -1 se il numero di righe non
     * è accettabile, -2 se il numero di colonne non è accettabile
     * @since 11/10/2019
     */
    public int setDimensioni(int Righe, int Colonne)
    {
        int nR = this.getNumeroDiRighe();
        if (Righe >= 1)
        {
            this.NumeroDiRighe = Righe;
        } else
        {
            return -1;
        }

        if (Colonne >= 1)
        {
            this.NumeroDiColonne = Colonne;
        } else
        {
            this.NumeroDiRighe = nR;
            return -2;
        }

        return 0;
    }

    /**
     * Inserisce in posizione Riga x Colonna l'elemento dato.
     *
     * @since 11/10/2019
     * @param Riga Il numero della riga dove inserire l'elemento
     * @param Colonna Il numero della colonna dove inserire l'elemento
     * @param Elemento L'elemento da inserire
     * @return 0 se il set va a buon fine, altrimenti -1
     */
    public int setElemento(int Riga, int Colonna, double Elemento)
    {
        if (Riga < this.getNumeroDiRighe() && Colonna < this.getNumeroDiColonne())
        {
            this.matrice[Riga][Colonna] = Elemento;
            return 0;
        }

        return -1;
    }
    ////////////////////////////////////////////////////////////////////

    /**
     * Stampa a video la matrice
     *
     * @since 11/10/2019
     */
    public void stampaAVideo()
    {
        int i, j;
        for (i = 0; i < this.getNumeroDiRighe(); i++)
        {
            for (j = 0; j < this.getNumeroDiColonne(); j++)
            {
                System.out.print(this.getElemento(i, j) + "  ");
                if (j != this.getNumeroDiColonne() - 1)
                {
                    System.out.print("| ");
                }
            }
            System.out.println("");
        }
    }

    /**
     * Stampa su un file il contenuto della matrice e stampa a video un
     * messaggio di errore se non riesce a salvare il file
     *
     * @param NomeFile Nome del file sul quale salvare la matrice
     * @param FileConDimensioni True se all'inizio del file voglio trovare anche
     * il numero di righe e di colonne
     * @return 0 se il salvataggio va a buon fine, altrimenti -1
     * @since 11/10/2019
     */
    public int ScritturaSuFile(String NomeFile, boolean FileConDimensioni)
    {
        BufferedWriter Writer;
        FileWriter File;
        int i, j;

        try
        {
            File = new FileWriter(NomeFile);
            Writer = new BufferedWriter(File);

            if (FileConDimensioni == true)
            {
                Writer.write(this.getNumeroDiRighe() + " " + this.getNumeroDiColonne() + "\r\n");
            }

            for (i = 0; i < this.getNumeroDiRighe(); i++)
            {
                for (j = 0; j < this.getNumeroDiColonne(); j++)
                {
                    Writer.write(this.getElemento(i, j) + "");
                    if (j != this.getNumeroDiColonne() - 1)
                    {
                        Writer.write(" ");
                    }
                }
                Writer.write("\r\n");
            }
            Writer.close();
            File.close();
            return 0;
        } catch (IOException ex)
        {
            System.err.println("Impossibile aprire il file");
            return -1;
        }
    }

    /**
     * Legge da file la matrice e la carica in memoriae stampa a video un
     * messaggio di errore se non riesce a caricare il file
     *
     * @param NomeFile Nome del file dal quale caricare la matrice
     * @param FileConDimensioni True se all'inizio del file trovo anche il
     * numero di righe e di colonne altrimenti false
     * @return 0 se la lettura va a buon fine, -1 se il file non contiene le
     * dimensioni della matrice, -2 se è impossibile aprire il file, -3 se è
     * impossibile chiudere il file
     * @since 14/10/2019
     */
    public int LetturaDaFile(String NomeFile, boolean FileConDimensioni)
    {
        FileReader reader;
        Scanner scanner;
        int i, j;

        try
        {
            reader = new FileReader(NomeFile);
            scanner = new Scanner(reader).useLocale(Locale.US);

            if (FileConDimensioni == true)
            {
                String Dim = scanner.nextLine();
                Scanner s1 = new Scanner(Dim).useLocale(Locale.US);

                int dim1 = 0, dim2 = 0;
                if (s1.hasNextInt())
                {
                    dim1 = s1.nextInt();
                }
                if (s1.hasNextInt())
                {
                    dim2 = s1.nextInt();
                }
                this.setDimensioni(dim1, dim2);

                if (s1.hasNextInt() || s1.hasNextDouble())
                {
                    System.err.println("Il file non contiene le dimensioni della matrice");
                    return -1;
                }
                s1.close();

                this.matrice = new double[this.NumeroDiRighe][this.NumeroDiColonne];

                for (i = 0; i < this.getNumeroDiRighe(); i++)
                {
                    for (j = 0; j < this.getNumeroDiColonne(); j++)
                    {
                        this.setElemento(i, j, scanner.nextDouble());
                    }
                }
            } else
            {
                int nR = 0, nC = 0;
                while (scanner.hasNextLine())
                {
                    if (nR == 0)
                    {
                        String s1 = scanner.nextLine();
                        Scanner riga = new Scanner(s1).useLocale(Locale.US);
                        while (riga.hasNextDouble())
                        {
                            riga.nextDouble();
                            nC++;
                        }
                    } else
                    {
                        scanner.nextLine();
                    }
                    nR++;
                }
                this.setDimensioni(nR, nC);
                this.matrice = new double[this.NumeroDiRighe][this.NumeroDiColonne];
                reader.close();
                scanner.close();

                reader = new FileReader(NomeFile);
                scanner = new Scanner(reader).useLocale(Locale.US);

                for (i = 0; i < this.getNumeroDiRighe(); i++)
                {
                    for (j = 0; j < this.getNumeroDiColonne(); j++)
                    {
                        this.setElemento(i, j, scanner.nextDouble());
                    }
                }
            }
            reader.close();
            scanner.close();
            return 0;
        } catch (FileNotFoundException ex)
        {
            System.err.println("Impossibile aprire il file");
            return -2;
        } catch (IOException ex)
        {
            System.err.println("Impossibile chiudere il file");
            return -3;
        }
    }

    /**
     * Ritorna in stringa la matrice, le colonne separate da spazi le righe da
     * \r\n
     *
     * @since 12/10/2019
     * @return In stringa la matrice preceduta dalle due dimensioni.
     */
    public String ScritturaSuStringa()
    {
        String str = "";

        str += this.getNumeroDiRighe() + " " + this.getNumeroDiColonne() + "\r\n";
        int i, j;
        for (i = 0; i < this.getNumeroDiRighe(); i++)
        {
            for (j = 0; j < this.getNumeroDiColonne(); j++)
            {
                str += this.getElemento(i, j) + "";
                if (j != this.getNumeroDiColonne() - 1)
                {
                    str += " ";
                }
            }
            str += "\r\n";
        }

        return str;
    }

    /**
     * Leggo da stringa la matrice e la carico in memoria.
     *
     * @param stringa La stringa dalla quale estrarre la matrice
     * @return 0 se la lettura è andata a buon fine, altrimenti -1
     *
     * @since 14/10/2019
     */
    public int LetturaDaStringa(String stringa)
    {
        Scanner s1 = new Scanner(stringa).useLocale(Locale.US);
        this.setDimensioni(s1.nextInt(), s1.nextInt());
        this.matrice = new double[this.getNumeroDiRighe()][this.getNumeroDiColonne()];
        int i, j = 0;

        for (i = 0; i < this.getNumeroDiRighe(); i++)
        {
            for (j = 0; j < this.getNumeroDiColonne() && s1.hasNextDouble(); j++)
            {
                this.setElemento(i, j, s1.nextDouble());
            }
        }
        if (i != this.getNumeroDiRighe() || j != this.getNumeroDiColonne())
        {
            return -1;
        }

        return 0;
    }

    /**
     * Scambio due righe della matrice
     *
     * @param NumeroRiga1 indice della prima riga
     * @param NumeroRiga2 indice della seconda riga
     * @return 0 se lo scambio va a buon fine, altrimenti -1
     * @since 14/10/2019
     */
    public int ScambiaRighe(int NumeroRiga1, int NumeroRiga2)
    {
        double arrayTemporaneo[];
        int i;

        arrayTemporaneo = new double[this.getNumeroDiRighe()];

        if (NumeroRiga1 < 0 || NumeroRiga1 > this.getNumeroDiRighe() - 1 || NumeroRiga2 < 0 || NumeroRiga2 > this.getNumeroDiRighe() - 1)
        {
            return -1;
        }

        for (i = 0; i < this.getNumeroDiColonne(); i++)
        {
            arrayTemporaneo[i] = this.getElemento(NumeroRiga1, i);
            this.setElemento(NumeroRiga1, i, this.getElemento(NumeroRiga2, i));
            this.setElemento(NumeroRiga2, i, arrayTemporaneo[i]);
        }

        return 0;
    }

    /**
     * Scambio due colonne della matrice
     *
     * @param NumeroColonna1 indice della prima colonna
     * @param NumeroColonna2 indice della seconda colonna
     * @return 0 se lo scambio va a buon fine, altrimenti -1
     * @since 14/10/2019
     */
    public int ScambiaColonne(int NumeroColonna1, int NumeroColonna2)
    {
        double arrayTemporaneo[];
        int i;

        arrayTemporaneo = new double[this.getNumeroDiColonne()];

        if (NumeroColonna1 < 0 || NumeroColonna1 > this.getNumeroDiColonne() - 1 || NumeroColonna2 < 0 || NumeroColonna2 > this.getNumeroDiColonne() - 1)
        {
            return -1;
        }

        for (i = 0; i < this.getNumeroDiRighe(); i++)
        {
            arrayTemporaneo[i] = this.getElemento(i, NumeroColonna1);
            this.setElemento(i, NumeroColonna1, this.getElemento(i, NumeroColonna2));
            this.setElemento(i, NumeroColonna2, arrayTemporaneo[i]);
        }

        return 0;
    }

    /**
     * Moltiplica una riga per un valore
     *
     * @param NumeroRiga il numero della riga da moltiplicare
     * @param Valore il valore(double) da moltiplicare
     * @return 0 se la moltiplicazione va a buon fine, altrimenti -1
     * @since 14/10/2019
     */
    public int MoltiplicaRiga(int NumeroRiga, double Valore)
    {
        if (NumeroRiga < 0 || NumeroRiga > this.getNumeroDiRighe() - 1)
        {
            return -1;
        }

        int i;

        for (i = 0; i < this.getNumeroDiColonne(); i++)
        {
            this.setElemento(NumeroRiga, i, this.getElemento(NumeroRiga, i) * Valore);
        }
        return 0;
    }

    /**
     * Moltiplica una colonna per un valore
     *
     * @param NumeroColonna il numero della colonna da moltiplicare
     * @param Valore il valore(double) da moltiplicare
     * @return 0 se la moltiplicazione va a buon fine, altrimenti -1
     * @since 14/10/2019
     */
    public int MoltiplicaColonna(int NumeroColonna, double Valore)
    {
        if (NumeroColonna < 0 || NumeroColonna > this.getNumeroDiColonne() - 1)
        {
            return -1;
        }

        int i;

        for (i = 0; i < this.getNumeroDiRighe(); i++)
        {
            this.setElemento(i, NumeroColonna, this.getElemento(i, NumeroColonna) * Valore);
        }
        return 0;
    }

    /**
     * Sommo due righe e sostituisco la prima con il risultato
     *
     * @param NumeroRiga1 il numero della prima riga
     * @param NumeroRiga2 il numero della seconda riga
     * @return 0 se la somma va a buon fine, altrimenti -1
     * @since 14/10/2019
     */
    public int SommaDueRighe(int NumeroRiga1, int NumeroRiga2)
    {
        if (NumeroRiga1 < 0 || NumeroRiga1 > this.getNumeroDiRighe() - 1 || NumeroRiga2 < 0 || NumeroRiga2 > this.getNumeroDiRighe() - 1)
        {
            return -1;
        }

        int i;

        for (i = 0; i < this.getNumeroDiColonne(); i++)
        {
            this.setElemento(NumeroRiga1, i, this.getElemento(NumeroRiga1, i) + this.getElemento(NumeroRiga2, i));
        }

        return 0;
    }

    /**
     * Sommo due colonne e sostituisco la prima con il risultato
     *
     * @param NumeroColonna1 il numero della prima colonna
     * @param NumeroColonna2 il numero della seconda colonna
     * @return 0 se la somma va a buon fine, altrimenti -1
     * @since 14/10/2019
     */
    public int SommaDueColonne(int NumeroColonna1, int NumeroColonna2)
    {
        if (NumeroColonna1 < 0 || NumeroColonna1 > this.getNumeroDiColonne() - 1 || NumeroColonna2 < 0 || NumeroColonna2 > this.getNumeroDiColonne() - 1)
        {
            return -1;
        }

        int i;

        for (i = 0; i < this.getNumeroDiRighe(); i++)
        {
            this.setElemento(i, NumeroColonna1, this.getElemento(i, NumeroColonna1) + this.getElemento(i, NumeroColonna2));
        }

        return 0;
    }

    /**
     * Somma due matrici
     *
     * @param m1 matrice 1
     * @param m2 matrice 2
     * @return una oggetto matrice contentente la somma delle due matrici
     * @since 25/10/2019
     */
    public static Matrice SommaMatrici(Matrice m1, Matrice m2)
    {
        if (m1.getNumeroDiRighe() != m2.getNumeroDiRighe() || m1.getNumeroDiColonne() != m2.getNumeroDiColonne())
        {
            return null;
        }

        Matrice ris = new Matrice(m1.getNumeroDiRighe(), m1.getNumeroDiColonne());
        int i, j;

        for (i = 0; i < ris.getNumeroDiRighe(); i++)
        {
            for (j = 0; j < ris.getNumeroDiColonne(); j++)
            {
                ris.setElemento(i, j, m1.getElemento(i, j) + m2.getElemento(i, j));
            }
        }

        return ris;
    }

    /**
     * Prodotto fra due matrici
     *
     * @param m1 La prima matrice
     * @param m2 La seconda matrice
     * @return Un oggetto matrice contenente il prodotto fra le due matrici,
     * null se non è possibile effettuare la moltiplicazione
     * @since 25/10/2019
     */
    public static Matrice ProdottoMatrici(Matrice m1, Matrice m2)
    {
        if (m1.getNumeroDiColonne() != m2.getNumeroDiRighe())
        {
            return null;
        }

        int i, j, k;
        Matrice ris = new Matrice(m1.getNumeroDiRighe(), m2.getNumeroDiColonne());

        for (i = 0; i < ris.getNumeroDiRighe(); i++)
        {
            for (j = 0; j < ris.getNumeroDiColonne(); j++)
            {
                double numero = 0;
                for (k = 0; k < m1.getNumeroDiColonne(); k++)
                {
                    numero = numero + m1.getElemento(i, k) * m2.getElemento(k, j);
                }
                ris.setElemento(i, j, numero);
            }
        }

        return ris;

    }

    /**
     * Calcola la matrice trasposta
     *
     * @param M la matrice di partenza
     * @return un oggetto matrice contenente la matice trasposta
     * @since 25/10/2019
     */
    public static Matrice Trasposta(Matrice M)
    {
        Matrice Ris = new Matrice(M.getNumeroDiColonne(), M.getNumeroDiRighe());

        int i, j;

        for (i = 0; i < Ris.getNumeroDiRighe(); i++)
        {
            for (j = 0; j < Ris.getNumeroDiColonne(); j++)
            {
                Ris.setElemento(i, j, M.getElemento(j, i));
            }
        }
        return Ris;
    }

    /**
     * Estrae un vettore riga da una matrice
     *
     * @param M1 La matrice dalla quale estrarre l'array
     * @param NumeroRiga Il numero della riga da estrarre
     * @return un array di double, null se non è possibile estrarre l'array
     * @since 25/10/2019
     */
    public static double[] EstrazioneVettoreRiga(Matrice M1, int NumeroRiga)
    {
        double array[];

        if (NumeroRiga > M1.getNumeroDiRighe() - 1 || NumeroRiga < 0)
        {
            return null;
        }

        array = new double[M1.getNumeroDiRighe()];

        int i;

        for (i = 0; i < M1.getNumeroDiColonne(); i++)
        {
            array[i] = M1.getElemento(NumeroRiga, i);
        }

        return array;
    }

    /**
     * Estrae un vettore cololonna da una matrice
     *
     * @param M1 La matrice dalla quale estrarre l'array
     * @param NumeroColonna Il numero della colonna da estrarre
     * @return un array di double, null se non è possibile estrarre l'array
     * @since 25/10/2019
     */
    public static double[] EstrazioneVettoreColonna(Matrice M1, int NumeroColonna)
    {
        double array[];

        if (NumeroColonna > M1.getNumeroDiColonne() - 1 || NumeroColonna < 0)
        {
            return null;
        }

        array = new double[M1.getNumeroDiColonne()];

        int i;

        for (i = 0; i < M1.getNumeroDiRighe(); i++)
        {
            array[i] = M1.getElemento(i, NumeroColonna);
        }

        return array;
    }

}
