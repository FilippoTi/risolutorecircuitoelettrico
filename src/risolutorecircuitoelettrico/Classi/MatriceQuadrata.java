package risolutorecircuitoelettrico.Classi;

import java.io.*;
import java.util.*;

public class MatriceQuadrata extends Matrice
{

    /**
     * Costruttore di default: Imposto le dimensioni a 1 1 e imposto l'elemento
     * a 0.0
     *
     * @since 22/10/2019
     */
    public MatriceQuadrata()
    {
        this.NumeroDiRighe = 1;
        this.NumeroDiColonne = 1;
        this.matrice = new double[NumeroDiRighe][NumeroDiRighe];
        this.matrice[0][0] = 0.0;
    }

    /**
     * Costruttore: Imposto le dimensioni della matrice quadrata passata come
     * paramentro e copio i valori, se la matrice passata non è quadrata imposto
     * le dimensioni a 1 1 e imposto il valore a 0
     *
     * @param matrice la matrice dalla quale prelevo i valori e le dimensioni
     * @since 22/10/2019
     */
    public MatriceQuadrata(double[][] matrice)
    {
        if (matrice.length > 0 && matrice[0].length > 0)
        {
            if (matrice.length == matrice[0].length)
            {
                this.matrice = matrice;
                this.NumeroDiRighe = matrice.length;
                this.NumeroDiColonne = matrice[0].length;
            } else
            {
                this.matrice = new double[1][1];
                matrice[0][0] = 0.0;
                this.NumeroDiRighe = 1;
                this.NumeroDiColonne = 1;
            }
        }
    }

    /**
     * Imposto la matrice quadrata di dimensione Ordine a 0.0
     *
     * @param Ordine l'ordine della mia matrice
     * @since 22/10/2019
     */
    public MatriceQuadrata(int Ordine)
    {
        this.NumeroDiRighe = Ordine;
        this.NumeroDiColonne = Ordine;
        this.matrice = new double[Ordine][Ordine];
        int i, j;
        for (i = 0; i < Ordine; i++)
        {
            for (j = 0; j < Ordine; j++)
            {
                this.matrice[i][j] = 0.0;
            }
        }
    }

    /////////////////////////////////////////////////////////Ereditati
    /**
     * Imposto la dimensione della matrice quadrata
     *
     * @param Ordine l'ordine della matrice
     * @since 22/10/2019
     * @return 0 se le dimensioni sono accettabli, -1 se le dimesnioni sono
     * minori o uguali a 0
     */
    public int setDimensioni(int Ordine)
    {
        if (Ordine >= 1)
        {
            this.NumeroDiRighe = Ordine;
            this.NumeroDiColonne = Ordine;
            return 0;
        } else
        {
            return -1;
        }
    }

    /**
     * DEPRECATO. Imposta le dimensioni della matrice quadrata
     *
     * @see #setDimensioni(int)
     * @param Righe Il numero di righe
     * @param Colonne Il numero di colonne
     *
     * @return 0 se l'operazione va a buon fine, -1 se le dimensioni sono
     * diverse
     * @deprecated 22/10/2019
     * @since 22/10/2019
     */
    @Override
    public int setDimensioni(int Righe, int Colonne)
    {
        if (Righe == Colonne)
        {
            this.setDimensioni(Righe);
            return 0;
        }
        System.err.println("Metodo non disponibile in questa classe");
        return -1;
    }

    /**
     * Legge da file la matrice, la carica in memoria, ritorna e stampa a video
     * un messaggio di errore se non riesce a caricare il file
     *
     * @param NomeFile Nome del file dal quale caricare la matrice
     * @param FileConDimensioni True se all'inizio del file trovo anche il
     * numero di righe e di colonne altrimenti false
     *
     * @return 0 se la lettura va a buon fine, -1 se il file non contiene le
     * dimensioni della matrice, -2 se è impossibile aprire il file, -3 se è
     * impossibile chiudere il file, -4 se il file non contiene una matrice
     * quadrata
     *
     * @since 25/10/2019
     */
    @Override
    public int LetturaDaFile(String NomeFile, boolean FileConDimensioni)
    {
        FileReader reader;
        Scanner scanner;
        int i, j;

        try
        {
            reader = new FileReader(NomeFile);
            scanner = new Scanner(reader).useLocale(Locale.US);

            if (FileConDimensioni == true)
            {
                int Dim1 = 1, Dim2 = 1;
                boolean HasDim = true;
                String Dims = scanner.nextLine();
                Scanner Tmp = new Scanner(Dims);

                if (Tmp.hasNextInt())
                {
                    Dim1 = Tmp.nextInt();
                    if (Tmp.hasNextInt())
                    {
                        Dim2 = Tmp.nextInt();
                    } else
                    {
                        HasDim = false;
                    }
                } else
                {
                    HasDim = false;
                }

                if (Tmp.hasNextInt() || HasDim == false)
                {
                    System.err.println("Il file non contiene le dimensioni della matrice");
                    return -1;
                }

                if (Dim1 != Dim2)
                {
                    System.err.println("Il file non contiene una matrice quadrata");
                    return -4;
                }
                this.setDimensioni(Dim1);

                this.matrice = new double[this.NumeroDiRighe][this.NumeroDiColonne];

                for (i = 0; i < this.getNumeroDiRighe(); i++)
                {
                    for (j = 0; j < this.getNumeroDiColonne(); j++)
                    {
                        this.setElemento(i, j, scanner.nextDouble());
                    }
                }
            } else
            {
                int nR = 0, nC = 0;
                while (scanner.hasNextLine())
                {
                    nC = scanner.nextLine().split(" ").length;
                    nR++;
                }
                if (nR != nC)
                {
                    System.err.println("Il file non contiene una matrice quadrata!");
                    return -4;
                }
                this.setDimensioni(nR);
                this.matrice = new double[this.NumeroDiRighe][this.NumeroDiColonne];
                reader.close();
                scanner.close();

                reader = new FileReader(NomeFile);
                scanner = new Scanner(reader);

                for (i = 0; i < this.getNumeroDiRighe(); i++)
                {
                    String strVal = scanner.nextLine();
                    String Vals[] = strVal.split(" ");
                    for (j = 0; j < this.getNumeroDiColonne(); j++)
                    {
                        this.setElemento(i, j, Double.parseDouble(Vals[j]));
                    }
                }
            }
            reader.close();
            scanner.close();
            return 0;
        } catch (FileNotFoundException ex)
        {
            System.err.println("Impossibile aprire il file");
            return -2;
        } catch (IOException ex)
        {
            System.err.println("Impossibile chiudere il file");
            return -3;
        }
    }

    /**
     * Leggo da stringa la matrice e la carico in memoria.
     *
     * @param stringa La stringa dalla quale estrarre la matrice
     * @return 0 se la lettura è andata a buon fine -1 se non riesco a riempire
     * completamente la matrice -2 se la matrice non è quadrata
     *
     * @since 25/10/2019
     */
    @Override
    public int LetturaDaStringa(String stringa)
    {
        Scanner s1 = new Scanner(stringa);
        int dim1, dim2;
        dim1 = s1.nextInt();
        dim2 = s1.nextInt();
        if (dim1 != dim2)
        {
            System.err.println("Matrice non quadrata!");
            return -2;
        }
        this.setDimensioni(dim1);
        this.matrice = new double[dim1][dim2];
        int i, j = 0;

        for (i = 0; i < this.getNumeroDiRighe(); i++)
        {
            for (j = 0; j < this.getNumeroDiColonne() && s1.hasNextDouble(); j++)
            {
                this.setElemento(i, j, s1.nextDouble());
            }
        }

        if (i != this.getNumeroDiRighe() || j != this.getNumeroDiColonne())
        {
            return -1;
        }

        return 0;
    }

    /**
     * Calcola la matrice trasposta
     *
     * @param M la matrice di partenza
     * @return un oggetto matriceQuadrata contenente la matice trasposta
     * @since 25/10/2019
     */
    public static MatriceQuadrata Trasposta(Matrice M)
    {
        MatriceQuadrata Ris = new MatriceQuadrata(M.getNumeroDiColonne());

        int i, j;

        for (i = 0; i < Ris.getNumeroDiRighe(); i++)
        {
            for (j = 0; j < Ris.getNumeroDiColonne(); j++)
            {
                Ris.setElemento(i, j, M.getElemento(j, i));
            }
        }
        return Ris;
    }
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Estrae una sottomatrice eliminando la riga Riga e la colonna Colonna,
     * stampa a video un messaggio e ritorna null in caso di errore.
     *
     * @param matrice La matrice da dove estrarre la sottomatrice
     * @param Riga La riga da eliminare
     * @param Colonna La colonna da eliminare
     *
     * @return Una matrice Quadrata di ordine -1 della matrice sorgente, null in
     * caso di errore
     *
     * @since 25/10/2019
     */
    public static MatriceQuadrata EstrazioneSottoMatrice(MatriceQuadrata matrice, int Riga, int Colonna)
    {
        int i, j;
        int iprimo = 0, jprimo;

        if (matrice == null)
        {
            System.err.println("Oggetto matrice vale null");
            return null;
        }

        MatriceQuadrata ris = new MatriceQuadrata(matrice.getNumeroDiRighe() - 1);

        if (matrice.getNumeroDiRighe() <= 1 || matrice.getNumeroDiColonne() <= 1)
        {
            System.err.println("Impossibile estrarre una sotto matrice da una matrice 1x1");
            return null;
        }

        if (Riga > matrice.getNumeroDiRighe() - 1 || Colonna > matrice.getNumeroDiColonne() - 1)
        {
            System.err.println("Impossibile estrarre una sotto matrice: riga o colonna non appartenente alla matrice");
            return null;
        }

        if (Riga < 0 || Colonna < 0)
        {
            System.err.println("Impossibile estrarre una sotto matrice: riga o colonna minori di 0");
            return null;
        }

        for (i = 0; i < matrice.getNumeroDiRighe(); i++)
        {
            jprimo = 0;
            for (j = 0; j < matrice.getNumeroDiColonne(); j++)
            {
                if (j != Colonna && i != Riga)
                {
                    ris.setElemento(iprimo, jprimo, matrice.getElemento(i, j));
                    jprimo++;
                }
            }
            if (i != Riga)
            {
                iprimo++;
            }
        }

        return ris;
    }

    /**
     * Calcola il determinante di una matrice quadrata
     *
     * @param matrice La matrice da cui estrarre il determinante
     * @return il determinante della matrice se tutto va a buon fine,
     * Double.MIN_VALUE in caso di ordine minore di 1 o di oggetto nullo
     *
     * @since 25/10/2019
     */
    public static double Determinante(MatriceQuadrata matrice)
    {
        if (matrice == null)
        {
            System.err.println("Oggetto matrice vale null");
            return Double.MIN_VALUE;
        }

        if (matrice.getNumeroDiRighe() == 1)
        {
            return matrice.getElemento(0, 0);
        }

        double ris = 0;

        if (matrice.getNumeroDiRighe() == 2)
        {
            ris = matrice.getElemento(0, 0) * matrice.getElemento(1, 1);
            ris = ris - matrice.getElemento(0, 1) * matrice.getElemento(1, 0);
            return ris;
        }

        int i;

        if (matrice.getNumeroDiRighe() > 2)
        {
            for (i = 0; i < matrice.getNumeroDiRighe(); i++)
            {
                ris = ris + (Math.pow(-1, i) * matrice.getElemento(i, 0) * MatriceQuadrata.Determinante(MatriceQuadrata.EstrazioneSottoMatrice(matrice, i, 0)));
            }

            return ris;
        }

        return Double.MIN_VALUE;
    }

    /**
     * Calcola la matrice inversa, scrive a video un messaggio in caso di errore
     * 
     * @param matrice la matrice di partenza
     * 
     * @return un Oggetto di tipo MatriceQuadrata se va tutto a buon fine, null
     * in caso di errore
     * 
     * @since 25/10/2019
     */
    public static MatriceQuadrata MatriceInversa(MatriceQuadrata matrice)
    {
        if (matrice == null)
        {
            System.err.println("Oggetto matrice nullo");
            return null;
        }

        if (matrice.getNumeroDiRighe() < 1)
        {
            System.err.println("Matrice vuota!");
            return null;
        }

        if (matrice.getNumeroDiRighe() == 1)
        {
            return matrice;
        }

        double D;
        if ((D = MatriceQuadrata.Determinante(matrice)) == 0)
        {
            System.err.println("Matrice con determinante 0");
            return null;
        }

        MatriceQuadrata ris = new MatriceQuadrata(matrice.getNumeroDiRighe());

        int i, j;

        for (i = 0; i < ris.getNumeroDiRighe(); i++)
        {
            for (j = 0; j < ris.getNumeroDiColonne(); j++)
            {
                ris.setElemento(i, j, (Math.pow(-1, i + j) * MatriceQuadrata.Determinante(MatriceQuadrata.EstrazioneSottoMatrice(matrice, i, j))));
            }
        }

        ris = MatriceQuadrata.Trasposta(ris);

        for (i = 0; i < ris.getNumeroDiRighe(); i++)
        {
            for (j = 0; j < ris.getNumeroDiColonne(); j++)
            {
                ris.setElemento(i, j, ris.getElemento(i, j) / D);
            }
        }

        return ris;

    }

}
